/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package httpservice;

import com.vng.jcore.common.Config;
import configuration.AppConfig;
import org.apache.log4j.Logger;
import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

/**
 *
 * @author ngaht
 */
public class WebServer {

    private static final Logger logger_ = Logger.getLogger(WebServer.class);

    public void start() throws Exception {
        Server server = new Server();

        //setup JMX
        MBeanContainer mbContainer = new MBeanContainer(java.lang.management.ManagementFactory.getPlatformMBeanServer());
        server.getContainer().addEventListener(mbContainer);
        server.addBean(mbContainer);
        mbContainer.addBean(Log.getLog());

        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(100);
        threadPool.setMaxThreads(2000);
        server.setThreadPool(threadPool);
        System.out.println(Config.getHomePath());

        SelectChannelConnector connector = new SelectChannelConnector();
        connector.setPort(AppConfig.PORT_LISTEN);
        connector.setMaxIdleTime(30000);
        connector.setConfidentialPort(8443);
        connector.setStatsOn(false);
        connector.setLowResourcesConnections(20000);
        connector.setLowResourcesMaxIdleTime(5000);

        server.setConnectors(new Connector[]{connector});


        ServletHandler handler = new ServletHandler();
        server.setHandler(handler);
        handler.addServletWithMapping("webservlet.UpBeaconController", AppConfig.API_CONTROLLER_PATH);
        handler.addServletWithMapping("webservlet.UpOpenBeaconController", AppConfig.OPEN_API_CONTROLLER_PATH);
        handler.addServletWithMapping("webservlet.TestController", AppConfig.OPEN_API_TEST_CONTROLLER_PATH);
	handler.addServletWithMapping("webservlet.UpOpenAdsController", AppConfig.OPEN_ADS_PATH);
        handler.addServletWithMapping("webservlet.PartnerController", AppConfig.OPEN_API_CONTROLLER_4Partner);
	
	server.setStopAtShutdown(true);
        server.setSendServerVersion(true);

        server.start();
        server.join();
    }
}
