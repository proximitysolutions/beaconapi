package service.log;

public class LogConstants {

    public static final String CATEGORY_MAPI_API_LOGIN = "ZINGAPP_MAPI_LOGIN_ACTION";
	public static final String CATEGORY_MAPI_API_LOGIN_REQUEST = "ZINGAPP_MAPI_LOGIN_REQUEST";
    public static final int MOBILE_GETACTIVECODE = 201;
    public static final int MOBILE_ACTIVECODE = 202;
    public static final int MOBILE_SENDSMS = 203;
    public static final int MOBILE_GETSMSREMAIN = 204;
    public static final int MOBILE_ACTIVECODE_USER = 205;
    public static final int MOBILE_UPDATE_PHONE_USER = 206;
	public static final int MOBILE_ACTIVE_PHONE_NEWACCOUNT = 207;
	public static final int MOBILE_REMOVE_MAPPING = 208;
    // session
    public static final int USER_DOLOGIN = 301;
    public static final int USER_DOSECURELOGIN = 302;
    public static final int USER_DOLOGOUT = 303;
    public static final int USER_DOLOGINMOBILEMAPPING = 304;
    public static final int USER_DOLOGINCHAT = 306;
    public static final int USER_DOLOGIN_FAILED = 307;
    public static final int USER_DOLOGIN_SECURE_CHAT = 308;
	public static final int USER_DOSECURELOGINMOBILEMAPPING = 309;
	public static final int USER_DOLOGINBYPHONE = 310;
	public static final int USER_CHANGEPASSWORD = 311;
	public static final int USER_MAPPINGMOBILE = 312;
	public static final int USER_GETMAPPINGFROMPHONE = 313;
	public static final int USER_GETMAPPINGFROMEMAILPHONE = 314;
	
	public static final int SOCIAL_REG =401;
}
