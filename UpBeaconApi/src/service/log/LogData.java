//package service.log;
//
//import service.utils.ServletUtil;
//import javax.servlet.http.HttpServletRequest;
////import service.models.Authentication;
//import service.models.data.ZSessionData;
//import sns.backend.transport.ScriberLog;
//
//public class LogData {
//    // ServerIP RequestDomain ClientIP Username ActionId Time AppData
//    // Execution_Time
//	public static String LOG_ACTION_TEMPLATE = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s";
//    
//	private String serverIp = "";
//    private String requestDomain = "";
//    private String clientIp = "";
//    private String username = "";
//    private String uri = "";
//    private String userAgent = "";
//    private int actionId = 0;
//    private String appData = "";
//    private long executionTime = 0;
//    private long startTime = 0;
//    private long endTime = 0;
//    private String category = "";
//	private String ostype = "";
//	private String deviceid = "";
//
//    public LogData(HttpServletRequest request, String category) {
//        super();
//        this.serverIp = request.getLocalAddr();
//        this.requestDomain = request.getServerName();
//
//        // client IP
//        String ip = request.getHeader("X-Forwarded-For");
//        if (ip != null && !ip.isEmpty()) {
//            this.clientIp = ip;
//        } else {
//            this.clientIp = request.getRemoteAddr();
//        }
//
//        this.uri = request.getRequestURI();
//        this.userAgent = request.getHeader("User-Agent");
//        this.startTime = System.currentTimeMillis();
//        this.category = category;
//
//        // username
//        String sessionKey = ServletUtil.getStringParameter(request, "session_key", "");
//        if (!sessionKey.isEmpty()) {
////            ZSessionData zsessiondata = Authentication.getInstance().getSessionData(sessionKey);
////            if(zsessiondata != null)
////            {
////                this.username = zsessiondata.userName;
////            }
//            
//        }
//		ostype = ServletUtil.getStringParameter(request, "ostype", "android");
//		deviceid = ServletUtil.getStringParameter(request, "deviceid", "");
//        appData = "MAPI2_LOGIN";
//    }
//
//	
//	public String getData() {
//		String data = String.format(LOG_ACTION_TEMPLATE, serverIp, requestDomain, clientIp, username, actionId, endTime,appData, ostype, deviceid, executionTime);
//		return data;
//	
//	}
//    public void sendLogApi(int actionId) {
//        this.actionId = actionId;
//        this.endTime = System.currentTimeMillis();
//        this.executionTime = this.endTime - this.startTime;
//
////        StringBuilder logString = new StringBuilder();
////        logString.append(this.serverIp).append('\t');
////        logString.append(this.requestDomain).append('\t');
////        logString.append(this.clientIp).append('\t');
////        logString.append(this.username).append('\t');
////        logString.append(this.actionId).append('\t');
////        logString.append(this.endTime).append('\t');
////        logString.append(this.appData).append('\t');
////		logString.append(this.ostype).append('\t');
////		logString.append(this.deviceid).append('\t');
////        logString.append(this.executionTime);
////		String str = logString.toString();
//		String str = getData();
//        ScriberLog.instance().writeLog(this.category, str);
//    }
//
//    public String getServerIp() {
//        return serverIp;
//    }
//
//    public void setServerIp(String serverIp) {
//        this.serverIp = serverIp;
//    }
//
//    public String getRequestDomain() {
//        return requestDomain;
//    }
//
//    public void setRequestDomain(String requestDomain) {
//        this.requestDomain = requestDomain;
//    }
//
//    public String getClientIp() {
//        return clientIp;
//    }
//
//    public void setClientIp(String clientIp) {
//        this.clientIp = clientIp;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public int getActionId() {
//        return actionId;
//    }
//
//    public void setActionId(int actionId) {
//        this.actionId = actionId;
//    }
//
//    public String getAppData() {
//        return appData;
//    }
//
//    public void setAppData(String appData) {
//        this.appData = appData;
//    }
//
//    public long getExecutionTime() {
//        return executionTime;
//    }
//
//    public void setExecutionTime(long executionTime) {
//        this.executionTime = executionTime;
//    }
//
//    public long getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(long startTime) {
//        this.startTime = startTime;
//    }
//
//    public long getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(long endTime) {
//        this.endTime = endTime;
//    }
//}