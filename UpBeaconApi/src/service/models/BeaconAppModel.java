/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.List;
import service.models.data.AppInfoData;
import service.models.data.AppInfoDataList;
import service.response.DataResponse;
import service.utils.Util;
import sns.backend.transport.BeaconIdListClient;
import sns.backend.transport.UpBeaconAppClient;
import vng.up.beacon.thrift.TAppInfo;

/**
 *
 * @author prophet
 */
public class BeaconAppModel {
    
    public static long genAppId(String appName) {
//        long currentTime = System.currentTimeMillis();
//        String key = AppConfig.mKeyHash + appName + String.valueOf(currentTime) + AppConfig.mKeyHash2;
//        String appId = Util.md5(key);
//        return appId;
        return IdGeneratorModel.genBeaconAppId(appName);
    }
    
    public static String genAppSecret(String appName) {
        long currentTime = System.currentTimeMillis();
        String key = AppConfig.mKeyHash2 + appName + String.valueOf(currentTime) + AppConfig.mKeyHash;
        String appId = Util.md5(key);
        return appId;
    }
    
    public static DataResponse addNewApp(String appName, String urlCall, boolean enable) {
        long appId = genAppId(appName);
        String secretKey = genAppSecret(appName);
        TAppInfo appInfo = new TAppInfo(appId, secretKey, appName, urlCall, enable);
        boolean result = UpBeaconAppClient.instance().put(appInfo);
        if(result) {
            AppInfoData appInfoData = new AppInfoData(appInfo);
            AppInfoData.AppInfoRespond appRespond = new AppInfoData.AppInfoRespond(result, appInfoData);
            
            BeaconIdListClient.instance().put(AppConfig.mAppBossId, appId);
            return new DataResponse(appRespond);
        } else {
            AppInfoData.AppInfoRespond appRespond = new AppInfoData.AppInfoRespond(result, null);
            return new DataResponse(appRespond);
        }
    }
    
    public static DataResponse updateApp(long appId, String appName, String urlCall, boolean enable) {
        TAppInfo appInfo = UpBeaconAppClient.instance().get(appId);
        if(appInfo.appId <= 0) {
            return DataResponse.PARAM_ERROR;
        }
        //TAppInfo appInfo = new TAppInfo(appId, secretKey, appName, urlCall, enable);
        appInfo.appName = appName;
        appInfo.urlCall = urlCall;
        appInfo.enable = enable;
        boolean result = UpBeaconAppClient.instance().put(appInfo);
        if(result) {
            AppInfoData appInfoData = new AppInfoData(appInfo);
            AppInfoData.AppInfoRespond appRespond = new AppInfoData.AppInfoRespond(result, appInfoData);
            
            BeaconIdListClient.instance().put(AppConfig.mAppBossId, appId);
            return new DataResponse(appRespond);
        } else {
            AppInfoData.AppInfoRespond appRespond = new AppInfoData.AppInfoRespond(result, null);
            return new DataResponse(appRespond);
        }
    }
    
    public static DataResponse updateName(long appId, String appName) {
        boolean result = UpBeaconAppClient.instance().updateName(appId, appName);
        return new DataResponse(result);
    }
    
    public static DataResponse updateUrl(long appId, String url) {
        boolean result = UpBeaconAppClient.instance().updateUrl(appId, url);
        return new DataResponse(result);
    }
    
    public static DataResponse updateState(long appId, boolean enable) {
        boolean result = UpBeaconAppClient.instance().updateState(appId, enable);
        return new DataResponse(result);
    }
    
    public static DataResponse getAppInfo(long appId) {
        TAppInfo appInfo = UpBeaconAppClient.instance().get(appId);
        if(appInfo == null) {
            return DataResponse.PARAM_ERROR;
        }else {
            AppInfoData appInfoData = new AppInfoData(appInfo);
            return new DataResponse(appInfoData);
        }
    }
    
    public static DataResponse getAppInfoList(int start, int length) {
        List<Long> appIdList = BeaconIdListClient.instance().getSlice(AppConfig.mAppBossId, start, length);
        if(appIdList != null) {
            List<TAppInfo> appInfoList = UpBeaconAppClient.instance().getAppInfoList(appIdList);
            if(appInfoList == null) {
                return DataResponse.PARAM_ERROR;
            }
            List<AppInfoData> appInfoDataList = new ArrayList<AppInfoData>();
            for (int i = 0; i< appInfoList.size(); i++) {
                TAppInfo appInfo = appInfoList.get(i);
                AppInfoData appInfoData = new AppInfoData(appInfo);
                appInfoDataList.add(appInfoData);
            }
            int total = BeaconIdListClient.instance().count(AppConfig.mAppBossId);
            AppInfoDataList modelList = new AppInfoDataList(total, appInfoDataList);
            return new DataResponse(modelList);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse doRemoveApp(long appId) {
        boolean result = UpBeaconAppClient.instance().remove(appId);
        if(result) {
            BeaconIdListClient.instance().remove(AppConfig.mAppBossId, appId);
        }
        return new DataResponse(result);
    }
}
