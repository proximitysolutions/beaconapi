/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.List;
import service.models.data.PartnerInfoData;
import service.models.data.PartnerInfoDataList;
import service.response.DataResponse;
import sns.backend.transport.Beacon2ParterListClient;
import sns.backend.transport.BeaconIdListClient;
import sns.backend.transport.Partner2BeaconListClient;
import sns.backend.transport.PartnerName2IdClient;
import sns.backend.transport.UpBeaconPartnerClient;
import vng.up.beacon.thrift.TPartnerInfo;

/**
 *
 * @author prophet
 */
public class BeaconPartnerModel {
    
    public static long genPartnerId(String name) {
//        long currentTime = System.currentTimeMillis();
//        String key = AppConfig.mKeyHash + name + String.valueOf(currentTime) + AppConfig.mKeyHash2;
//        String appId = Util.md5(key);
//        return appId;
        return IdGeneratorModel.genBeaconPartnerId(name);
    }
    
    public static DataResponse addNewPartner(String name, String address, boolean enable, String username, String password) {
        // check username exist
        long id = PartnerName2IdClient.instance().get(username);
        if(id > 0) {
            return DataResponse.CHECK_VALUE_USERNAME_EXIST;
        }
        long partnerId = genPartnerId(name);
        TPartnerInfo partnerInfo = new TPartnerInfo(partnerId, name, address, enable, username, password);
        boolean result = UpBeaconPartnerClient.instance().put(partnerInfo);
        if(result) {
            PartnerInfoData partnerInfoData = new PartnerInfoData(partnerInfo);
            int numberBeacon = Partner2BeaconListClient.instance().count(partnerInfo.partnerId);
            partnerInfoData.numberBeacon = numberBeacon;
            PartnerInfoData.PartnerInfoRespond partnerRespond = new PartnerInfoData.PartnerInfoRespond(result, partnerInfoData);
            
            BeaconIdListClient.instance().put(AppConfig.mPartnerBossId, partnerId);
            PartnerName2IdClient.instance().put(username, partnerId);
            
            return new DataResponse(partnerRespond);
        } else {
            PartnerInfoData.PartnerInfoRespond appRespond = new PartnerInfoData.PartnerInfoRespond(result, null);
            return new DataResponse(appRespond);
        }
    }
    
    public static DataResponse updatePartner(long partnerId, String name, String address, boolean enable) {
        //TPartnerInfo partnerInfo = new TPartnerInfo(partnerId, name, address, enable);
        TPartnerInfo partnerInfo = UpBeaconPartnerClient.instance().get(partnerId);
        if(partnerInfo.partnerId <= 0) {
            return DataResponse.PARAM_ERROR;
        }
        partnerInfo.name = name;
        partnerInfo.address = address;
        partnerInfo.enable = enable;
        boolean result = UpBeaconPartnerClient.instance().put(partnerInfo);
        if(result) {
            PartnerInfoData partnerInfoData = new PartnerInfoData(partnerInfo);
            int numberBeacon = Partner2BeaconListClient.instance().count(partnerInfo.partnerId);
            partnerInfoData.numberBeacon = numberBeacon;
            PartnerInfoData.PartnerInfoRespond partnerRespond = new PartnerInfoData.PartnerInfoRespond(result, partnerInfoData);
            return new DataResponse(partnerRespond);
        } else {
            PartnerInfoData.PartnerInfoRespond appRespond = new PartnerInfoData.PartnerInfoRespond(result, null);
            return new DataResponse(appRespond);
        }
    }
    
    public static DataResponse updateName(long partnerId, String name) {
        boolean result = UpBeaconPartnerClient.instance().updateName(partnerId, name);
        return new DataResponse(result);
    }
    
    public static DataResponse updateAddress(long partnerId, String address) {
        boolean result = UpBeaconPartnerClient.instance().updateAddress(partnerId, address);
        return new DataResponse(result);
    }
    
    public static DataResponse updateState(long partnerId, boolean enable) {
        boolean result = UpBeaconPartnerClient.instance().updateState(partnerId, enable);
        return new DataResponse(result);
    }
    
    public static DataResponse getPartnerInfo(long partnerId) {
        TPartnerInfo partnerInfo = UpBeaconPartnerClient.instance().get(partnerId);
        if(partnerInfo == null) {
            return DataResponse.PARAM_ERROR;
        }else {
            PartnerInfoData partnerInfoData = new PartnerInfoData(partnerInfo);
            return new DataResponse(partnerInfoData);
        }
    }
    
    public static DataResponse getPartnerInfoList(int start, int length) {
        List<Long> partnerIdList = BeaconIdListClient.instance().getSlice(AppConfig.mPartnerBossId, start, length);
        if(partnerIdList != null) {
            List<TPartnerInfo> partnerInfoList = UpBeaconPartnerClient.instance().getPartnerInfoList(partnerIdList);
            if(partnerInfoList == null)
                return DataResponse.PARAM_ERROR;
            List<PartnerInfoData> partnerModelList = new ArrayList<PartnerInfoData>();
            for(int i = 0; i< partnerInfoList.size(); i++) {
                TPartnerInfo partnerInfo = partnerInfoList.get(i);
                PartnerInfoData partnerInfoData = new PartnerInfoData(partnerInfo);
                int numberBeacon = Partner2BeaconListClient.instance().count(partnerInfo.partnerId);
                partnerInfoData.numberBeacon = numberBeacon;
                partnerModelList.add(partnerInfoData);
            }
            int total = BeaconIdListClient.instance().count(AppConfig.mPartnerBossId);
            PartnerInfoDataList returnList = new PartnerInfoDataList(total, partnerModelList);
            return new DataResponse(returnList);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse doGetPartnerFromBeacon(long beaconId, int start, int length) {
//        List<Long> partnerIdList = BeaconIdListClient.instance().getSlice(AppConfig.mPartnerBossId, start, length);
        List<Long> partnerIdList = Beacon2ParterListClient.instance().getSlice(beaconId, start, length);
        if(partnerIdList != null) {
            System.out.println("so partnerId: " + partnerIdList.size());
            if(partnerIdList.size() >0) {
                for (Long partnerId : partnerIdList) {
                    System.out.print("partnerId: " + partnerId);
                }
            }
            List<TPartnerInfo> partnerInfoList = UpBeaconPartnerClient.instance().getPartnerInfoList(partnerIdList);
            if(partnerInfoList == null)
                return DataResponse.PARAM_ERROR;
            List<PartnerInfoData> partnerModelList = new ArrayList<PartnerInfoData>();
            for(int i = 0; i< partnerInfoList.size(); i++) {
                TPartnerInfo partnerInfo = partnerInfoList.get(i);
                PartnerInfoData partnerInfoData = new PartnerInfoData(partnerInfo);
                int numberBeacon = Partner2BeaconListClient.instance().count(partnerInfo.partnerId);
                partnerInfoData.numberBeacon = numberBeacon;
                partnerModelList.add(partnerInfoData);
            }
            int total = Beacon2ParterListClient.instance().count(beaconId);
            PartnerInfoDataList returnList = new PartnerInfoDataList(total, partnerModelList);
            return new DataResponse(returnList);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    
    
    public static DataResponse doRemovePartner(long partnerId) {
        boolean result = UpBeaconPartnerClient.instance().remove(partnerId);
        if(result) {
            BeaconIdListClient.instance().remove(AppConfig.mPartnerBossId, partnerId);
            
            List<Long> beaconIdList = Partner2BeaconListClient.instance().getListAll(partnerId);
            if(beaconIdList != null && beaconIdList.size() >0) {
                for (Long beaconId : beaconIdList) {
                    BeaconModel.doRemovePartnerFromBeacon(beaconId, partnerId);
                }
            }
        }
        return new DataResponse(result);
    }
}
