/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import sns.backend.transport.BeaconName2IdClient;
import sns.backend.transport.GeneratorClient;

/**
 *
 * @author prophet
 */
public class IdGeneratorModel {
    public static long genBeaconId(String beaconName) {
        if(! AppConfig.mIsGenBeaconName) {
            int result = GeneratorClient.instance().createGenerator(AppConfig.mGenBeaconName);
            if(result >= 0) {
                AppConfig.mIsGenBeaconName = true;
            }
        }
        long beaconId = GeneratorClient.instance().getValue(AppConfig.mGenBeaconName);
        if(beaconId >0) {
            BeaconName2IdClient.instance().put(beaconName, beaconId);
        }
        return beaconId;
    }
    
    public static long genBeaconAppId(String appName) {
        if(! AppConfig.mIsGenAppName) {
            int result = GeneratorClient.instance().createGenerator(AppConfig.mGenAppName);
            if(result >= 0) {
                AppConfig.mIsGenAppName = true;
            }
        }
        long appId = GeneratorClient.instance().getValue(AppConfig.mGenAppName);
//        if(appId >0) {
//            BeaconName2IdClient.instance().put(appName, appId);
//        }
        return appId;
    }
    
    public static long genBeaconPartnerId(String name) {
        if(! AppConfig.mIsGenParterName) {
            int result = GeneratorClient.instance().createGenerator(AppConfig.mGenParterName);
            if(result >= 0) {
                AppConfig.mIsGenParterName = true;
            }
        }
        long partnerId = GeneratorClient.instance().getValue(AppConfig.mGenParterName);
//        if(appId >0) {
//            BeaconName2IdClient.instance().put(appName, appId);
//        }
        return partnerId;
    }
    
    public static long genAdvertisementId() {
        if(! AppConfig.mIsGenAdvertisementName) {
            int result = GeneratorClient.instance().createGenerator(AppConfig.mGenAdvertisementName);
            if(result >= 0) {
                AppConfig.mIsGenAdvertisementName = true;
            }
        }
        long adId = GeneratorClient.instance().getValue(AppConfig.mGenAdvertisementName);
//        if(appId >0) {
//            BeaconName2IdClient.instance().put(appName, appId);
//        }
        return adId;
    }
    
    public static long genDeviceId() {
        if(! AppConfig.mIsGenDeviceIdName) {
            int result = GeneratorClient.instance().createGenerator(AppConfig.mGenDeviceIdName);
            if(result >= 0) {
                AppConfig.mIsGenDeviceIdName = true;
            }
        }
        long deviceId = GeneratorClient.instance().getValue(AppConfig.mGenDeviceIdName);
        return deviceId;
    }
}
