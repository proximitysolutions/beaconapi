/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import service.models.data.AdvertisementInfoData;
import service.models.data.BeaconInfoData;
import service.models.data.BeaconInfoDataList;
import service.response.DataResponse;
import service.utils.HttpRequestUtils;
import sns.backend.transport.Beacon2AdListClient;
import sns.backend.transport.Beacon2DeviceViewAdListClient;
import sns.backend.transport.Beacon2ParterListClient;
import sns.backend.transport.UpBeaconAppClient;
import vng.up.beacon.thrift.TAppInfo;
import sns.backend.transport.BeaconLocationClient;
import sns.backend.transport.Device2IdClient;
import sns.backend.transport.Partner2BeaconListClient;
import sns.backend.transport.UpAdvertisementClient;
import sns.backend.transport.UpBeaconClient;
import sns.backend.transport.UserLocationClient;
import vng.up.beacon.location.thrift.TUserDistance;
import vng.up.beacon.thrift.TAdvertisementInfo;
import vng.up.beacon.thrift.TBeaconInfo;

/**
 *
 * @author prophet
 */
public class OpenApiModel {

    public static int NUMBER_BEACON_GET = 20;
    public static double BEACON_DISTANCE_RADIUS = 0.5; // 500 MET

    public static DataResponse deviceNearBeacon(String deviceIdStr, long beaconId, long appId, String deviceIdentify) {
        long deviceId = Device2IdClient.instance().get(deviceIdStr);
        if(deviceId <= 0) {
            deviceId = IdGeneratorModel.genDeviceId();
            if(deviceId >0) {
                Device2IdClient.instance().put(deviceIdStr, deviceId);
            }
        }
        
        if (beaconId < 0 || appId < 0) {
            return DataResponse.PARAM_ERROR;
        }
        TAppInfo appInfo = UpBeaconAppClient.instance().get(appId);
        if (appInfo == null) {
            return DataResponse.PARAM_ERROR;
        }
        String url = appInfo.urlCall;
        String adUrl = AppConfig.BASE_URL_STRING + AppConfig.OPEN_ADS_PATH.replace("*", "") + beaconId + "/" + deviceId;

        String adsString = "";
        List<Long> lastestAds = Beacon2AdListClient.instance().getSliceReverse(beaconId, 0, 1);
        if (lastestAds != null && lastestAds.size() > 0) {
            long lastestId = lastestAds.get(0);
            TAdvertisementInfo oneAds = UpAdvertisementClient.instance().get(lastestId);
            if (oneAds != null && oneAds.advertisementId > 0) {
                adsString = oneAds.title + " : " + oneAds.content;
            }
        }

        System.out.println("urlCall: " + url);
        System.out.println("adUrl " + adUrl);
        if (url != null && url.length() > 5) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("beaconId", "" + beaconId);
            params.put("appId", "" + appId);
            params.put("deviceIdentify", deviceIdentify);
            params.put("toGetAdsUrl", adUrl);
            params.put("lastestAds", adsString);
            try {
                HttpRequestUtils.sendHttpRequest2(url, "POST", params);
            } catch (Exception ex) {
            }
            return new DataResponse(params);
        }
        return DataResponse.PARAM_ERROR;
    }

    public static DataResponse listBeaconAtLocation(String deviceIdStr, double longtitude, double latitude, long appId) {
        long deviceId = Device2IdClient.instance().get(deviceIdStr);
        if(deviceId <= 0) {
            deviceId = IdGeneratorModel.genDeviceId();
            if(deviceId >0) {
                Device2IdClient.instance().put(deviceIdStr, deviceId);
            }
        }
        System.out.println("deviceId: " + deviceId);
        if(deviceId > 0) {
            UserLocationClient.instance().add(latitude, longtitude, deviceId);
        }
        List<BeaconInfoData> modelDataList = new ArrayList<BeaconInfoData>();
        List<Long> beaconIds = BeaconLocationClient.instance().searchUsers(latitude, longtitude, BEACON_DISTANCE_RADIUS, 0, NUMBER_BEACON_GET);
        if (beaconIds == null || beaconIds.size() == 0) {
            System.out.println("search result : " + beaconIds);
            return new DataResponse(modelDataList);
        }

        List<TBeaconInfo> listBeaconInfo = UpBeaconClient.instance().getBeaconInfoList(beaconIds);
        if (listBeaconInfo == null || listBeaconInfo.size() == 0) {
            System.out.println("get beacon Info = null with ids : " + beaconIds);
            return new DataResponse(modelDataList);
        }

        for (int i = 0; i < listBeaconInfo.size(); i++) {
            TBeaconInfo beaconInfo = listBeaconInfo.get(i);
            if (beaconInfo == null || beaconInfo.beaconId <= 0) {
                System.out.println("beconInfo = null || beaconInfo.beaconId < 0 :" + beaconInfo);
                continue;
            }
            BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
            modelDataList.add(modelData);
        }

        return new DataResponse(modelDataList);
    }

    public static DataResponse getListAdOfBeacon(long beaconId, long deviceId) {
        List<Long> listAdIds = Beacon2AdListClient.instance().getListAll(beaconId);
        List<AdvertisementInfoData> _returnData = new ArrayList<AdvertisementInfoData>();

        if (listAdIds == null || listAdIds.isEmpty()) {
            System.err.println("listAdIds " + listAdIds);
            return new DataResponse(_returnData);
        }
        List<TAdvertisementInfo> listAdInfor = UpAdvertisementClient.instance().getAdvertisementInfoList(listAdIds);

        if (listAdInfor == null || listAdInfor.isEmpty()) {
            System.err.println("listAdIds " + listAdIds + " listAdinfor " + listAdInfor);
            return new DataResponse(_returnData);
        }
        for (int i = 0; i < listAdInfor.size(); i++) {
            TAdvertisementInfo adInfor = listAdInfor.get(i);
            if (adInfor != null && adInfor.advertisementId > 0) {
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfor);
                _returnData.add(adInfoData);
            } else {
                System.err.println("adInfor " + adInfor);
            }
        }
        Beacon2DeviceViewAdListClient.instance().insert(beaconId, deviceId);
        return new DataResponse(_returnData);
    }

//    public static void main(String[] args) throws Exception {
//        float lng = (float) 105.86862962;
//        float lat = (float) 21.01706134;
//
//        DataResponse _return = OpenApiModel.listBeaconAtLocation(lng, lat, 9);
//        System.out.println("_retunr " + _return);
//        System.out.println("long " + lng + " lat " + lat);
//        List<vng.up.beacon.location.thrift.TUserDistance> beaconIds = BeaconLocationClient.instance().searchUsersWithDistance(lat, lng, BEACON_DISTANCE_RADIUS, 0, NUMBER_BEACON_GET);
//        System.out.println("return list " + beaconIds);
//    }
}
