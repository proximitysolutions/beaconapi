/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import vng.up.beacon.thrift.TAdvertisementInfo;

/**
 *
 * @author prophet
 */
public class AdvertisementInfoData {

    public long advertisementId; // required
    public String title; // required
    public String content; // required
    public String startTime; // required
    public String endTime; // required
    public String adsImageUrl;

    public AdvertisementInfoData(TAdvertisementInfo adInfo) {
        this.advertisementId = adInfo.advertisementId;
        this.title = adInfo.title;
        this.content = adInfo.content;

        Date date = new Date(adInfo.startTime);
        this.startTime = new SimpleDateFormat("dd/MM/yyyy").format(date);
        date = new Date(adInfo.endTime);
        this.endTime = new SimpleDateFormat("dd/MM/yyyy").format(date);
        this.adsImageUrl = randoomImage();
    }

    public static class AdvertisementInfoResponse {

        public boolean result;
        public AdvertisementInfoData adInfo;

        public AdvertisementInfoResponse(boolean result, AdvertisementInfoData adInfo) {
            this.result = result;
            this.adInfo = adInfo;
        }
    };

    private static String randoomImage() {
        int randoom = (int) (Math.random() * 5);
        if (randoom == 0) {
            return "http://img.apps.zdn.vn/4763.jpg";
        } else if (randoom == 1) {
            return "http://img.apps.zdn.vn/4753.jpg";
        } else if (randoom == 2) {
            return "http://img.apps.zdn.vn/4749.jpg";
        } else if (randoom == 3) {
            return "http://img.apps.zdn.vn/4739.jpg";
        }
        return "http://img.apps.zdn.vn/4775.jpg";
    }
//
//    public static void main(String[] args) throws Exception {
//        String imageString = randoomImage();
//        System.err.println(imageString);
//        imageString = randoomImage();
//        System.err.println(imageString);
//        imageString = randoomImage();
//        System.err.println(imageString);
//        imageString = randoomImage();
//        System.err.println(imageString);
//
//    }

}
