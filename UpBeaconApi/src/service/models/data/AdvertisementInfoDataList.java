/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import java.util.List;

/**
 *
 * @author prophet
 */
public class AdvertisementInfoDataList {
    public int total;
    public List<AdvertisementInfoData> advertisements;
    
    public AdvertisementInfoDataList(int total, List<AdvertisementInfoData> advertisements) {
        this.total = total;
        this.advertisements = advertisements;
    }
}
