/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import vng.up.beacon.thrift.TAppInfo;

/**
 *
 * @author prophet
 */
public class AppInfoData {

    public long appId; // required
    public String secretKey; // required
    public String appName; // required
    public String urlCall; // required
    public int enable; // required
    
    public AppInfoData(TAppInfo appInfo) {
        this.appId = appInfo.appId;
        this.secretKey = appInfo.secretKey;
        this.appName = appInfo.appName;
        this.urlCall = appInfo.urlCall;
        this.enable = appInfo.enable ? 1 : 0;
    }
    
    public static class AppInfoRespond {
        public boolean result;
        public AppInfoData appInfo;
        
        public AppInfoRespond(boolean result, AppInfoData appInfoData) {
            this.result = result;
            this.appInfo = appInfoData;
        }
    }
}
