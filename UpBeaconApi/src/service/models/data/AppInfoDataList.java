/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import java.util.List;

/**
 *
 * @author prophet
 */
public class AppInfoDataList {
    public int total;
    public List<AppInfoData> apps;
    
    public AppInfoDataList(int total, List<AppInfoData> appInfoDataList) {
        this.total = total;
        this.apps = appInfoDataList;
    }
}
