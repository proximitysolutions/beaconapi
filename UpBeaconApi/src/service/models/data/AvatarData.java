package service.models.data;

import service.utils.Util;



public class AvatarData {

    private static final int[] sizes = {50, 75, 120, 160, 180};
    private static final String[] domains = {"avatar.me.zdn.vn", "s120.avatar.zdn.vn", "s120.avatar.zdn.vn", "s160.avatar.zdn.vn", "b2.avatar.zdn.vn"};

    private static int getAvatarSizeAndDomainIndex(Integer avatarSize) {
        if (avatarSize == null) {
            return 0;
        }

        int index = 0;

        for (int i = 1; i < sizes.length; i++) {
            if (sizes[i] <= avatarSize) {
                index = i;
            } else {
                break;
            }
        }

        return index;
    }

    // Derived from UserProfile.buildAvatarPath
    public static String getAvatarPath(String userName, short avatarVersion, int avatarSize) {
        
        int index = getAvatarSizeAndDomainIndex(avatarSize);

        String path = domains[index];
        int level = 4;
        String hash = Util.md5(userName);
        String avatarpath = "";

        for (int i = 0; i < level; i++) {
            avatarpath += "/" + hash.substring(i, i + 1);
        }

        return "http://" + path + avatarpath + "/" + userName + "_" + avatarSize + "_"
                + avatarVersion + ".jpg";
    }
}
