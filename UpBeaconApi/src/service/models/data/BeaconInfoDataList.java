/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import java.util.List;

/**
 *
 * @author prophet
 */
public class BeaconInfoDataList {
    public int total;
    public List<BeaconInfoData> beacons;
    
    public BeaconInfoDataList(int total, List<BeaconInfoData> beacons) {
        this.total = total;
        this.beacons = beacons;
    }
}
