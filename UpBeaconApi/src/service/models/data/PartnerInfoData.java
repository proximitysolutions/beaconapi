/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import vng.up.beacon.thrift.TPartnerInfo;

/**
 *
 * @author prophet
 */
public class PartnerInfoData {

    public long partnerId; // required
    public String name; // required
    public String address; // required
    public int enable; // required
    public String username;
    public String password;
    public int numberBeacon;

    public PartnerInfoData(TPartnerInfo partnerInfo) {
        this.partnerId = partnerInfo.partnerId;
        this.name = partnerInfo.name;
        this.address = partnerInfo.address;
        this.username = partnerInfo.username;
        this.password = partnerInfo.password;
        this.enable = partnerInfo.enable ? 1 : 0;
    }
    
    public static class PartnerInfoRespond {
        public boolean result;
        public PartnerInfoData partnerInfo;
        
        public PartnerInfoRespond(boolean result, PartnerInfoData partnerInfoData) {
            this.result = result;
            this.partnerInfo = partnerInfoData;
        }
    }
}
