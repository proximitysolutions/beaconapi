/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import java.util.List;

/**
 *
 * @author prophet
 */
public class PartnerInfoDataList {
    public int total;
    public List<PartnerInfoData> partners;
    
    public PartnerInfoDataList(int total, List<PartnerInfoData> partners) {
        this.total = total;
        this.partners = partners;
    }
}
