/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

/**
 *
 * @author prophet
 */
public class PutBeaconData {
    public boolean result;
    public BeaconInfoData beaconInfo;
    
    public PutBeaconData(boolean result, BeaconInfoData beaconInfo) {
        this.result = result;
        this.beaconInfo = beaconInfo;
    }
}
