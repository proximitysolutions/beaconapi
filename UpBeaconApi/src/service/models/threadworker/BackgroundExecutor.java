/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.threadworker;

import configuration.AppConfig;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author tungvc
 */
public class BackgroundExecutor {
	private static ExecutorService _executor = Executors.newFixedThreadPool(AppConfig.N_BACKGROUND_THREAD);
	public static ExecutorService get() {
		return _executor;
	}
	
//	private static EmotionListenWorker _emotionListen = new EmotionListenWorker();
//	public static  EmotionListenWorker emotionListen(){
//		return _emotionListen;
//	}
//	
//	static
//	{
//		EmotionListenWorker.initWorkerWarmup();
//	}
}
