package service.response;

import com.vng.wmb.lib.json.JacksonHelper;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.ObjectMapper;

@JsonPropertyOrder({"error", "message", "data"})
public class DataResponse {

    @JsonProperty("error")
    private final int errorCode;
    @JsonProperty("message")
    private final String errorMessage;
    @JsonProperty("data")
    private Object data;
    private DataResponse.DataType dataType = DataResponse.DataType.NORMAL;
    private boolean isEscape = true;

    public DataResponse(int error, String message) {
        this.errorCode = error;
        this.errorMessage = message;
    }

    public DataResponse(int error, String message, String data) {
        this.errorCode = error;
        this.errorMessage = message;
        this.data = data;
    }

    public DataResponse(Object data) {
        this.errorCode = 0;
        this.errorMessage = "Successful.";
        this.data = data;
    }

    public DataResponse(Object data, DataResponse.DataType d, boolean isEscape) {
        this.errorCode = 0;
        this.errorMessage = "Successful.";
        this.data = data;
        this.dataType = d;
        this.isEscape = isEscape;
    }

    @JsonIgnore
    public int getError() {
        return this.errorCode;
    }

    @JsonIgnore
    public String getMessage() {
        return this.errorMessage;
    }

    @JsonIgnore
    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.setData(data, DataResponse.DataType.NORMAL);
    }

    public void setData(Object data, DataResponse.DataType dataType) {
        this.data = data;
        this.dataType = dataType;
    }

    @JsonIgnore
    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    @JsonIgnore
    public boolean isEscape() {
        return isEscape;
    }

    public void setEscape(boolean isEscape) {
        this.isEscape = isEscape;
    }

    @Override
    public String toString() {
        return DataResponse.toJsonString(this);
    }

    /**
     * ********************* STATIC ***************************************
     */
    public enum DataType {

        NORMAL, JSON_STR, UNSURE
    };
    // 100
    public static final DataResponse CHECK_VALUE_VALID = new DataResponse(0, "value is valid");
    
    public static final DataResponse CHECK_VALUE_INVALID = new DataResponse(1, "value is invalid");
    public static final DataResponse CHECK_VALUE_EMAIL_EXIST = new DataResponse(2, "The email is existed");
    public static final DataResponse CHECK_VALUE_EMAIL_INVALID = new DataResponse(6, "The email is invalid");
    public static final DataResponse CHECK_VALUE_USERNAME_EXIST = new DataResponse(3, "Username is existed");
    public static final DataResponse CREATE_ACCOUNT_PASSWORD_TOO_SHORT = new DataResponse(4, "password is too short");
    public static final DataResponse CREATE_ACCOUNT_ERROR = new DataResponse(5, "create account error");
    public static final DataResponse CREATE_ACCOUNT_PUT_PROFILE_ERROR = new DataResponse(7, "put profile error");
    
    public static final DataResponse LOGIN_NOT_EXIST_EMAIL = new DataResponse(1, "email is not exist");
    public static final DataResponse LOGIN_NOT_EXIST_USERNAME = new DataResponse(2, "username is not exist");
    public static final DataResponse LOGIN_USER_ERROR = new DataResponse(3, "login has error");
    public static final DataResponse LOGIN_WRONG_EMAIL_OR_PASSWORD = new DataResponse(4, "login has error");
    
    public static final DataResponse GET_ACTIVE_CODE_PHONE_FORMAT = new DataResponse(1, "sai dinh dang so dien thoai nhe ku");
    public static final DataResponse GET_ACTIVE_CODE_LIMIT_IN_DAY = new DataResponse(2, "vuot qua gioi han lay ma kich hoat trong ngay");
    public static final DataResponse ACTIVE_CODE_LIMIT = new DataResponse(10, "limited");
    public static final DataResponse ACTIVE_CODE_NOT_FOUND = new DataResponse(11, "not found code");
    public static final DataResponse ACTIVE_CODE_TIME_OUT = new DataResponse(12, "code is timeout");
    
    
    public static final DataResponse INVALID_PUBLIC_KEY = new DataResponse(101, "The API key submitted is not associated with any known application");
    public static final DataResponse SESSION_EXPIRE = new DataResponse(102, "The session key was improperly submitted or has reached its timeout. Direct the user to log in again to obtain another key");
    public static final DataResponse INVALID_SIGNATURE = new DataResponse(104, "Incorect signature");
    public static final DataResponse METHOD_NOT_FOUND = new DataResponse(110, "Method not found");
    public static final DataResponse MISSING_PARAM = new DataResponse(111, "One or more required parameter is not present");
    public static final DataResponse UNKNOWN_EXCEPTION = new DataResponse(112, "Unknown exception");
    public static final DataResponse PERMISSION_EXCEPTION = new DataResponse(113, "Your application is not allowed to use this API");
    public static final DataResponse BANNED_EXCEPTION = new DataResponse(114, "User is banned");
    // 200
    public static final DataResponse EXCEED_LOGIN_TIMES = new DataResponse(201, "This account attempts to try many login failed. Please retry next 3 mins");
    // 300
    public static final DataResponse INVALID_UPLOAD = new DataResponse(324, "Missing or invalid upload file");
    public static final DataResponse ITEM_NOT_FOUND = new DataResponse(1001, "Item not found");
    public static final DataResponse API_RESPONSE_EMPTY = new DataResponse(1002, "API's Response is empty");
    public static final DataResponse FORMAT_ERROR = new DataResponse(1003, "Format Error");
    public static final DataResponse PARAM_ERROR = new DataResponse(1004, "Param Error");
    public static final DataResponse GET_CODE_ERROR = new DataResponse(1005, "Get Code Error");
    public static final DataResponse ACTIVE_PHONE_ERROR = new DataResponse(1006, "Active Phone Error");
    public static final DataResponse ACTIVE_PHONE_CREATE_NEW_ACC_ERROR = new DataResponse(1007, "Active Phone Create New Account Error");
    public static final DataResponse REMOVE_MAPPING_ERROR = new DataResponse(1008, "Remove mapping error");
    public static final DataResponse LOGIN_ERROR = new DataResponse(1009, "Login Error");
    public static final DataResponse WAIT_LOGIN_AGAIN = new DataResponse(1010, "Wait to login again");
    public static final DataResponse WAIT_2_LOGIN_AGAIN = new DataResponse(1011, "wait 2s to login");
    public static final DataResponse ERR_LIMIT_ACTIVE_CODE = new DataResponse(1012, "error limit active code");
    public static final DataResponse ERR_AUTH_PHONE_CODE = new DataResponse(1013, "error auth phone code");
    public static final DataResponse ERR_DEF = new DataResponse(1014, "error def");
    public static final DataResponse ERR_DISABLE_ACTIVE = new DataResponse(1015, "disable active");
    public static final DataResponse ERR_FROMAT_PHONE = new DataResponse(1016, "fromat phone");
    public static final DataResponse ERR_GET_CODE = new DataResponse(1017, "get code active");
    public static final DataResponse ERR_GET_PPMAPPING = new DataResponse(1018, "get mapping");
    public static final DataResponse ERR_LIMIT_GET_CODE = new DataResponse(1019, "error limit get code");
    public static final DataResponse ERR_WAIT_ACTIVE_AGAIN = new DataResponse(1020, "wait active again");
    public static final DataResponse ERR_CHANGE_PWD = new DataResponse(1021, "error change pass");

    public static DataResponse getSuccessMsg() {
        return new DataResponse(0, "Successful.");
    }

//    public static String toJsonString(DataResponse apiMessage) {
//        try {
//            JSONPObject jsonDataResponse = new JSONPObject();
//            jsonDataResponse..put("error_code", apiMessage.errorCode);
//            jsonDataResponse.put("error_message", apiMessage.errorMessage);
//            jsonDataResponse.put("data", apiMessage.data);
//            return jsonDataResponse.toJSONString();
//            
//        } catch (Exception e) {
//        }
//
//        return "";
//    }
    public static String toJsonString(DataResponse dataResponse) {
        try {
            ObjectMapper mapper = null;

            if (dataResponse.isEscape()) {
                mapper = JacksonHelper.getEscapedInstance();
            } else {
                mapper = JacksonHelper.getUnescapedInstance();
            }

            if (mapper != null) {
                if (dataResponse.getDataType() == DataResponse.DataType.JSON_STR) {
                    return "{\"error_code\":" + dataResponse.getError() + ",\"error_message\":\"" + dataResponse.getMessage() + "\", \"data\":" + dataResponse.getData() + "}";
                } else if (dataResponse.getDataType() == DataResponse.DataType.UNSURE && dataResponse.getData() instanceof String && isJsonString((String) dataResponse.getData())) {
                    return "{\"error_code\":" + dataResponse.getError() + ",\"error_message\":\"" + dataResponse.getMessage() + "\", \"data\":" + dataResponse.getData() + "}";
                }

                // DATA_TYPE_NORMAL
                return mapper.writeValueAsString(dataResponse);
            }
        } catch (Exception e) {
        }

        return "";
    }

    public static boolean isJsonString(String str) {
        try {
            ObjectMapper mapper = JacksonHelper.getUnescapedInstance();
            mapper.readTree(str);

            return true;
        } catch (Exception e) {
        }

        return false;
    }
}