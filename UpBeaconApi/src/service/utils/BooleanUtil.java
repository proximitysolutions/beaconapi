package service.utils;

public class BooleanUtil {

    public static boolean valueOf(String boolStr) {
        if (boolStr == null || boolStr.isEmpty()) {
            return false;
        } else if ("1".equals(boolStr) || "true".equals(boolStr)) {
            return true;
        } else {
            return false;
        }
    }
}
