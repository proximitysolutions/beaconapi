package service.utils;

import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DecryptAES {

    private static final Logger logger = LoggerFactory.getLogger(DecryptAES.class);

    public static byte[] decryptPass(String encrytedStr, String username, String aesKey) {
        String key = Util.md5(aesKey + username).substring(5, 21);
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");

        byte[] rawByte = hexToByte(encrytedStr);

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");

            cipher.init(Cipher.DECRYPT_MODE, keySpec);

            byte[] decrypted = cipher.doFinal(rawByte);

            return decrypted;
        } catch (Exception e) {
            logger.error("Exception decryptPass: " + e.getMessage());
        }

        return null;
    }

    public static String getToken(String encrytedStr, String strPhoneNumber, String aesKey) {
        byte[] decrypted = decryptPass(encrytedStr, strPhoneNumber, aesKey);

        ByteArrayOutputStream bb = new ByteArrayOutputStream();
        for (int i = 0; i < decrypted.length; i++) {
            byte bt = decrypted[i];
            // Range 0-10: 48->57
            // Range A-Z: 65->90
            // Range a-z: 97->122
            if (bt >= 48 && bt <= 57 || bt >= 65 && bt <= 90 || bt >= 97 && bt <= 122) {
                bb.write(bt);
            } else {
                break;
            }
        }

        return new String(bb.toByteArray());
    }

    private static byte[] hexToByte(String hex) {
        ByteArrayOutputStream bb = new ByteArrayOutputStream();

        for (int i = 0; i < hex.length(); i += 2) {
            bb.write((byte) Integer.parseInt(hex.substring(i, i + 2), 16));
        }

        return bb.toByteArray();
    }

    public static String getPassword(String key, String password) {
        key = key.substring(5, 21);

        AES128 aes = new AES128();
        aes.init(key.getBytes());
        byte[] encrypt = new byte[16];
        byte[] input = null;

        int count = password.length() / 16 + 1;
        String temp = "";
        for (int i = 0; i < count; i++) {
            input = new byte[16];
            if (i == count - 1) {
                System.arraycopy(password.getBytes(), 16 * i, input, 0, password.length() - i * 16);
            } else {
                System.arraycopy(password.getBytes(), 16 * i, input, 0, 16);
            }
            if (aes.processBlock(input, 0, encrypt, 0) == 16) {
                temp += asHex(encrypt);
            }
        }
        return temp;
    }

    
    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if ((buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString(buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }
    public static void main(String[] args) {
		

		String aesKey = "72623d68ad4209436a75ce36a6b76610";

		//String key = MD5.digest(aesKey + username);
		//String encrytedStr = PasswordUtils.getPassword(key, password);
		
		String data= "AA22BFB005944B2E092DA3F0374C4235";
		
        SecretKeySpec keySpec = new SecretKeySpec(data.getBytes(), "AES");

        byte[] rawByte = hexToByte(aesKey);

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");

            cipher.init(Cipher.DECRYPT_MODE, keySpec);

            byte[] decrypted = cipher.doFinal(rawByte);

			ByteArrayOutputStream bb = new ByteArrayOutputStream();
        for (int i = 0; i < decrypted.length; i++) {
            byte bt = decrypted[i];
            // Range 0-10: 48->57
            // Range A-Z: 65->90
            // Range a-z: 97->122
            if (bt >= 48 && bt <= 57 || bt >= 65 && bt <= 90 || bt >= 97 && bt <= 122) {
                bb.write(bt);
            } else {
                break;
            }
        }

       String n = new String(bb.toByteArray());
	   int x = 0;
        } catch (Exception e) {
            logger.error("Exception decryptPass: " + e.getMessage());
        }

		
        
			
	}
//
//	public static void testToken() {
//		String username = "0909251393";
//		String aesKey = "e343a00dc94c7510996fec527b88b23d";
//
//		String encrytedStr = "d33ae812ae2c7bb0b72a6a0b344d4ac7";
//		String strToken = getToken(encrytedStr, username, aesKey);
//
//		System.out.println(strToken);
//
//		long token = ZGenerator.decodeHash(strToken);
//		System.out.println(token);
//	}
    /*	public static void main(String[] args) {
     mapi();
     System.out.println("");
     //		testToken();
     System.exit(1);
     }*/
}
