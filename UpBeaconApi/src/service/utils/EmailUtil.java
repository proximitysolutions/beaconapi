/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utils;

/**
 *
 * @author prophet
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailUtil {

    private final Pattern pattern;

    private Matcher matcher;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public EmailUtil() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    public boolean validate(final String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private static EmailUtil _instance;
    public static EmailUtil getIntance(){
        if(_instance == null)
            _instance = new EmailUtil();
        return _instance;
    }
}
