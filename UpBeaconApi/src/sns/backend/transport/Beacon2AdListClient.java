/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class Beacon2AdListClient extends I64ListI64Client {
    private static final Beacon2AdListClient m_instance = new Beacon2AdListClient();

    public static Beacon2AdListClient instance() {
        return m_instance;
    }

    @Override
    public TClientInfo getClientInfo() {
        return ClientFactory.getClient(
                AppConfig.mBeacon2AdListHost, AppConfig.mBeacon2AdListPort,
                vng.up.core.list.i64i64.I64ListI64Service.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
    }
}
