/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 *
 * @author prophet
 */
public class BeaconName2IdClient extends StringI64Client {
    private static final BeaconName2IdClient m_instance = new BeaconName2IdClient();

    public static BeaconName2IdClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    @Override
    public TClientInfo getClientInfo() {
        return ClientFactory.getClient(
                AppConfig.mBeaconName2IdHost, AppConfig.mBeaconName2IdPort,
                vng.up.core.map.stringi64.StringI64Service.Client.class,
                TBinaryProtocol.class);
    }
}
