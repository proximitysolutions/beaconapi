/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class I32StringClient {
    private static final I32StringClient m_instance = new I32StringClient();

    public static I32StringClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    public TClientInfo getClientInfo() {
        //System.out.println("host: " + AppConfig.mId2PhoneHost + " port: " + AppConfig.mId2PhonePort);
        String host = "127.0.0.1";
        int port = 9010;
        return ClientFactory.getClient(
                host, port,
                vng.up.core.map.i32string.I32StringService.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
    }
    
    public boolean put(int key, String entry){
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.map.i32string.I32StringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.put(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.put(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
    
    public String get(int key){
        String _return = "";
        TClientInfo aInfo = getClientInfo();
        vng.up.core.map.i32string.I32StringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.get(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.get(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
}
