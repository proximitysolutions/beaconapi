/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import java.util.List;
import java.util.Map;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import vng.up.core.list.i64i64.TValue;

/**
 *
 * @author prophet
 */
public class I64ListI64Client {

    private static final I64ListI64Client m_instance = new I64ListI64Client();

    public static I64ListI64Client instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    public TClientInfo getClientInfo() {
        //System.out.println("host: " + AppConfig.mId2SessionListHost + " port: " + AppConfig.mId2SessionListPort);

        String host = "127.0.0.1";
        int port = 9010;
        return ClientFactory.getClient(
                host, port,
                vng.up.core.list.i64i64.I64ListI64Service.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
    }

    public int count(long key) {
        int _return = -1;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.count(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.count(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public boolean existed(long key, long entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.existed(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.existed(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public List<Long> getListAll(long key) {
        List<Long> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getListAll(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getListAll(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public List<Long> getSlice(long key, int start, int length) {
        List<Long> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSlice(key, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSlice(key, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<Long> getSliceFromEntry(long key, long entryStart, int length) {
        List<Long> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSliceFromEntry(key, entryStart, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSliceFromEntry(key, entryStart, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<Long> getSliceReverse(long key, int start, int length) {
        List<Long> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSliceReverse(key, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSliceReverse(key, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<Long> getSliceFromEntryReverse(long key, long entryStart, int length) {
        List<Long> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSliceFromEntryReverse(key, entryStart, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSliceFromEntryReverse(key, entryStart, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public boolean put(long key, long entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.put(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.put(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public boolean putValue(long key, TValue value) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.putValue(key, value);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.putValue(key, value);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public boolean putList(long key, List<Long> entryList) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.putList(key, entryList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.putList(key, entryList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public boolean insert(long key, long entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.insert(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.insert(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean insertAt(long key, long entry, int index) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.insertAt(key, entry, index);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.insertAt(key, entry, index);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean remove(long key, long entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.remove(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.remove(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean removeAt(long key, int index) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.removeAt(key, index);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.removeAt(key, index);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean bumpUp(long key, long entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.bumpUp(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.bumpUp(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean removeList(long key, List<Long> entryList) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.removeList(key, entryList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.removeList(key, entryList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public void clearData(long key) {
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                aClient.clearData(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        aClient.clearData(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
    }

    
    public long split(long oldKey, long newKey, int origLength) {
        long _return = -1;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.split(oldKey, newKey, origLength);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.split(oldKey, newKey, origLength);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<Long, Integer> multiCount(List<Long> keyList) {
        Map<Long, Integer> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiCount(keyList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiCount(keyList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<Long, Boolean> multiExisted(List<Long> keyList, long entry) {
        Map<Long, Boolean> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiExisted(keyList, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiExisted(keyList, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<Long, List<Long>> multiGetListAll(List<Long> keyList) {
        Map<Long, List<Long>> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiGetListAll(keyList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiGetListAll(keyList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<Long, List<Long>> multiGetSlice(List<Long> keyList, int start, int length) {
        Map<Long, List<Long>> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiGetSlice(keyList, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiGetSlice(keyList, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<Long, List<Long>> multiGetSliceReverse(List<Long> keyList, int start, int length) {
        Map<Long, List<Long>> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiGetSliceReverse(keyList, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiGetSlice(keyList, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<Long, Boolean> multiPut(List<Long> keyList, long entry) {
        Map<Long, Boolean> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i64i64.I64ListI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiPut(keyList, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiPut(keyList, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
}
