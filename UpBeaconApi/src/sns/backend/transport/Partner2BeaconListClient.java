/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class Partner2BeaconListClient extends I64ListI64Client {
    private static final Partner2BeaconListClient m_instance = new Partner2BeaconListClient();

    public static Partner2BeaconListClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    @Override
    public TClientInfo getClientInfo() {
        return ClientFactory.getClient(
                AppConfig.mPartner2BeaconListHost, AppConfig.mPartner2BeaconListPort,
                vng.up.core.list.i64i64.I64ListI64Service.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
    }
}
