/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 *
 * @author prophet
 */
public class PartnerName2IdClient extends StringI64Client {
    private static final PartnerName2IdClient m_instance = new PartnerName2IdClient();

    public static PartnerName2IdClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    @Override
    public TClientInfo getClientInfo() {
        return ClientFactory.getClient(
                AppConfig.mPartnerName2IdHost, AppConfig.mPartnerName2IdPort,
                vng.up.core.map.stringi64.StringI64Service.Client.class,
                TBinaryProtocol.class);
    }
}
