/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;
import org.socialnetwork.snssession.*;
import configuration.AppConfig;
import jcommon.transport.client.*;

/**
 *
 * @author trungthanh
 */
public class SNSSessionClient {
    private static final SNSSessionClient m_instance = new SNSSessionClient();
    
    public static SNSSessionClient instance()
    {
        return m_instance;
    }
    
    public TClientInfo getClientInfo()
    {
        TClientInfo aInfo = ClientFactory.getClient(
                AppConfig.mSessionForPartnerHost, AppConfig.mSessionForPartnerPort
                ,org.socialnetwork.snssession.SNSSessionService.Client.class
                ,org.apache.thrift.protocol.TCompactProtocol.class
                );
        if (aInfo != null)
            aInfo.sureOpen();
        return aInfo;
    }
    
    public org.socialnetwork.snssession.TSessionInfoResult getSession(String snsauth) {
        TClientInfo aInfo = getClientInfo();
        TSessionInfoResult _return = null;
        org.socialnetwork.snssession.SNSSessionService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getSession(snsauth);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getSession(snsauth);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;

    }
    
     public void addSession(String snsauth, String username, int uid)
     {
        TClientInfo aInfo = getClientInfo();
        org.socialnetwork.snssession.SNSSessionService.Client aClient = aInfo.getClientT();
        try {
            aClient.addSession(snsauth, username, uid);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                aClient.addSession(snsauth, username, uid);

            } catch (Exception e2) {
            }

        }         
    
        aInfo.cleanUp();

     }
     
      public void removeSession(String snsauth) {
        TClientInfo aInfo = getClientInfo();
        org.socialnetwork.snssession.SNSSessionService.Client aClient = aInfo.getClientT();
        try {
            aClient.removeSession(snsauth);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                aClient.removeSession(snsauth);

            } catch (Exception e2) {
            }

        }         
    
        aInfo.cleanUp();
          
      }
    
}
