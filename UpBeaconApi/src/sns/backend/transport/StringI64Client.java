/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class StringI64Client {
    private static final StringI64Client m_instance = new StringI64Client();

    public static StringI64Client instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    public TClientInfo getClientInfo() {
        //System.out.println("host: " + AppConfig.mId2PhoneHost + " port: " + AppConfig.mId2PhonePort);
        String host = "127.0.0.1";
        int port = 9010;
        return ClientFactory.getClient(
                host, port,
                vng.up.core.map.stringi64.StringI64Service.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
        
    }
    
    public boolean put(String key, long entry){
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.map.stringi64.StringI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.put(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.put(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
    
    public long get(String key){
        long _return = -2;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.map.stringi64.StringI64Service.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.get(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.get(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
}
