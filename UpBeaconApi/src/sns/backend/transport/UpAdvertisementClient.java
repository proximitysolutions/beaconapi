/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.List;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import vng.up.beacon.thrift.TAdvertisementInfo;

/**
 *
 * @author prophet
 */
public class UpAdvertisementClient {

    private static final UpAdvertisementClient m_instance = new UpAdvertisementClient();

    public static UpAdvertisementClient instance() {
        return m_instance;
    }

    public TClientInfo getClientInfo() {
        TClientInfo aInfo = ClientFactory.getClient(
                AppConfig.mAdvertisementHost, AppConfig.mAdvertisementPort, vng.up.beacon.thrift.UpAdvertisementService.Client.class, org.apache.thrift.protocol.TBinaryProtocol.class
        );
        if (aInfo != null) {
            aInfo.sureOpen();
        }
        return aInfo;
    }

    public boolean put(TAdvertisementInfo advertisementInfo) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpAdvertisementService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.put(advertisementInfo);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.put(advertisementInfo);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateTitle(long adId, String title) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpAdvertisementService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateTitle(adId, title);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateTitle(adId, title);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateContent(long adId, String content) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpAdvertisementService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateContent(adId, content);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateContent(adId, content);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean remove(long adId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpAdvertisementService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.remove(adId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.remove(adId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public TAdvertisementInfo get(long adId) {
        TClientInfo aInfo = getClientInfo();
        TAdvertisementInfo _return = null;
        vng.up.beacon.thrift.UpAdvertisementService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.get(adId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.get(adId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public List<TAdvertisementInfo> getAdvertisementInfoList(List<Long> adIdList) {
        TClientInfo aInfo = getClientInfo();
        List<TAdvertisementInfo> _return = null;
        vng.up.beacon.thrift.UpAdvertisementService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getAdvertisementInfoList(adIdList);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getAdvertisementInfoList(adIdList);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
}
