/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.List;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import vng.up.beacon.thrift.TAppInfo;

/**
 *
 * @author prophet
 */
public class UpBeaconAppClient {

    private static final UpBeaconAppClient m_instance = new UpBeaconAppClient();

    public static UpBeaconAppClient instance() {
        return m_instance;
    }

    public TClientInfo getClientInfo() {
        TClientInfo aInfo = ClientFactory.getClient(
                AppConfig.mBeaconAppHost, AppConfig.mBeaconAppPort, vng.up.beacon.thrift.UpBeaconAppService.Client.class, org.apache.thrift.protocol.TBinaryProtocol.class
        );
        if (aInfo != null) {
            aInfo.sureOpen();
        }
        return aInfo;
    }

    public boolean put(TAppInfo appInfo) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.put(appInfo);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.put(appInfo);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateName(long appId, String name) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateName(appId, name);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateName(appId, name);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateUrl(long appId, String url) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateUrl(appId, url);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateUrl(appId, url);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateState(long appId, boolean enable) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateState(appId, enable);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateState(appId, enable);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean remove(long appId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.remove(appId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.remove(appId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public TAppInfo get(long appId) {
        TClientInfo aInfo = getClientInfo();
        TAppInfo _return = null;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.get(appId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.get(appId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
    
    public List<TAppInfo> getAppInfoList(List<Long> appIdList) {
        TClientInfo aInfo = getClientInfo();
        List<TAppInfo> _return = null;
        vng.up.beacon.thrift.UpBeaconAppService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getAppInfoList(appIdList);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getAppInfoList(appIdList);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
}
