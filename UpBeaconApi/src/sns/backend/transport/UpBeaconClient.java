/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.List;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import vng.up.beacon.thrift.TBeaconInfo;

/**
 *
 * @author prophet
 */
public class UpBeaconClient {

    private static final UpBeaconClient m_instance = new UpBeaconClient();

    public static UpBeaconClient instance() {
        return m_instance;
    }

    public TClientInfo getClientInfo() {
        TClientInfo aInfo = ClientFactory.getClient(
                AppConfig.mBeaconHost, AppConfig.mBeaconPort, vng.up.beacon.thrift.UpBeaconService.Client.class, org.apache.thrift.protocol.TBinaryProtocol.class
        );
        if (aInfo != null) {
            aInfo.sureOpen();
        }
        return aInfo;
    }

    public boolean put(TBeaconInfo beaconInfo) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.put(beaconInfo);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.put(beaconInfo);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean multiPut(List<TBeaconInfo> beaconInfoList) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.multiPut(beaconInfoList);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.multiPut(beaconInfoList);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean addPartner(long beaconId, long partnerId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.addPartner(beaconId, partnerId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.addPartner(beaconId, partnerId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean removePartner(long beaconId, long partnerId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.removePartner(beaconId, partnerId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.removePartner(beaconId, partnerId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateLocation(long beaconId, double lat, double lng) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateLocation(beaconId, lat, lng);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateLocation(beaconId, lat, lng);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateState(long beaconId, boolean enable) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateState(beaconId, enable);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateState(beaconId, enable);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean remove(long beaconId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.remove(beaconId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.remove(beaconId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public TBeaconInfo getBeaconInfo(long beaconId) {
        TClientInfo aInfo = getClientInfo();
        TBeaconInfo _return = null;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getBeaconInfo(beaconId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getBeaconInfo(beaconId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
    
    public List<TBeaconInfo> getBeaconInfoList(List<Long> beaconIdList) {
        TClientInfo aInfo = getClientInfo();
        List<TBeaconInfo> _return = null;
        vng.up.beacon.thrift.UpBeaconService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getBeaconInfoList(beaconIdList);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getBeaconInfoList(beaconIdList);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
}


