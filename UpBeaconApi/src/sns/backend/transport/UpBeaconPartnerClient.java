/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.List;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import vng.up.beacon.thrift.TPartnerInfo;

/**
 *
 * @author prophet
 */
public class UpBeaconPartnerClient {

    private static final UpBeaconPartnerClient m_instance = new UpBeaconPartnerClient();

    public static UpBeaconPartnerClient instance() {
        return m_instance;
    }

    public TClientInfo getClientInfo() {
        TClientInfo aInfo = ClientFactory.getClient(
                AppConfig.mBeaconPartnerHost, AppConfig.mBeaconPartnerPort, vng.up.beacon.thrift.UpBeaconPartnerService.Client.class, org.apache.thrift.protocol.TBinaryProtocol.class
        );
        if (aInfo != null) {
            aInfo.sureOpen();
        }
        return aInfo;
    }

    public boolean put(TPartnerInfo partnerInfo) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.put(partnerInfo);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.put(partnerInfo);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateName(long partnerId, String name) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateName(partnerId, name);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateName(partnerId, name);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateAddress(long partnerId, String address) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateAddress(partnerId, address);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateAddress(partnerId, address);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean updateState(long partnerId, boolean enable) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.updateState(partnerId, enable);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.updateState(partnerId, enable);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean addBeacon(long partnerId, long beaconId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.addBeacon(partnerId, beaconId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.addBeacon(partnerId, beaconId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean removeBeacon(long partnerId, long beaconId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.removeBeacon(partnerId, beaconId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.removeBeacon(partnerId, beaconId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean remove(long partnerId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.remove(partnerId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.remove(partnerId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public TPartnerInfo get(long partnerId) {
        TClientInfo aInfo = getClientInfo();
        TPartnerInfo _return = null;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.get(partnerId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.get(partnerId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
    
    public List<TPartnerInfo> getPartnerInfoList(List<Long> partnerIdList) {
        TClientInfo aInfo = getClientInfo();
        List<TPartnerInfo> _return = null;
        vng.up.beacon.thrift.UpBeaconPartnerService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getPartnerInfoList(partnerIdList);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getPartnerInfoList(partnerIdList);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
}
