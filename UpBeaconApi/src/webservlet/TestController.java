/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservlet;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
//import service.models.PushNotificationTestModel;
import service.response.DataResponse;
import service.utils.ServletUtil;

/**
 *
 * @author prophet
 */
public class TestController extends BaseServer {

    private static final Logger log = Logger.getLogger(TestController.class);

    @Override
    protected void doProcess(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        DataResponse dataResponse  = doProcessNearBeacon(request);
        if (dataResponse != null) {
            dataResponse.setEscape(true);
            out(dataResponse.toString(), resp);
        }
    }

    private DataResponse doProcessNearBeacon(HttpServletRequest req) {
        if (checkValidParam(req, new String[]{"beaconId", "appId"})) {
            long beaconId = ServletUtil.getLongParameter(req, "beaconId");
            String appId = ServletUtil.getStringParameter(req, "appId");
            String deviceIdentify = ServletUtil.getStringParameter(req, "deviceIdentify");
            String toGetAdsUrl = ServletUtil.getStringParameter(req, "toGetAdsUrl");
            String lastestAds = ServletUtil.getStringParameter(req, "lastestAds");
            System.out.println(lastestAds);
            if (deviceIdentify != null && deviceIdentify.length() > 0) {
                pushAPNSMessage(lastestAds, deviceIdentify, toGetAdsUrl);
            } else {
                System.out.println("deviceIdentify  = null");
            }

        } else {
            return DataResponse.MISSING_PARAM;
        }
        return DataResponse.MISSING_PARAM;
    }

    private void pushAPNSMessage(String message, String deviceToken, String adsUrl) {
        System.out.println("send apns message : " + message);
        try {

            String deviceIdentify = deviceToken.replace("<", "");
            deviceIdentify = deviceIdentify.replace(">", "");
            System.out.println("deviceToken " + deviceToken);

            ApnsService service
                    = APNS.newService()
                    .withCert("./cer/Certificates.p12", "123456")
                    .withSandboxDestination()
                    .build();

            String payload = APNS.newPayload()
                    .badge(1)
                    .customField("adsDetailUrl", adsUrl)
                    .localizedKey(message)
                    .build();

            service.push(deviceIdentify, payload);
            service.stop();

        } catch (Exception e) {
            System.err.println("ex " + e);
        }
    }

    public static void main(String[] args) throws Exception {
        String token = "<e4a09518 8298de4c 81b490dd f9655dbb 0574e686 5f2cc5d4 a8fa5441 274b8c52>";
        TestController controller = new TestController();
        controller.pushAPNSMessage("Chim se goi dai bang 2", token, "me.zing.vn");
    }
}
