/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.log.LogConstants;
import service.models.AdvertisementModel;
import service.models.BeaconAppModel;
import service.models.BeaconModel;
import service.models.BeaconPartnerModel;
//import service.log.LogData;
import service.response.DataResponse;
import service.utils.ServletUtil;

/**
 *
 * @author prophet
 */
public class UpBeaconController extends BaseServer{
    private static final Logger log = Logger.getLogger(UpBeaconController.class);
    
    @Override
    protected void doProcess(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        //LogData logEntry = new LogData(request, LogConstants.CATEGORY_MAPI_API_LOGIN);
        resp.addHeader("Access-Control-Allow-Origin", "*");
//        resp.addHeader("Access-Control-Allow-Origin:", "*");
//        resp.addHeader("Access-Control-Allow-Origin: *", "*");
        DataResponse dataResponse = null;
        String method = ServletUtil.getStringParameter(request, "method");
        if (method != null) {
            method = method.replace(".", "_").toLowerCase();
        }
        
//        if("login_checkvalue".equals(method)){
//            dataResponse = doCheckValue(request);
//        }else if("login_createaccount".equals(method)){
//            dataResponse = doCreateAccount(request);
//        }else if("login_addmobilenumber".equals(method)){
//            dataResponse = doGetActiveCode(request);
//        }else if("login_activatemobilenumber".equals(method)){
//            dataResponse = doActiveMobileCode(request);
//        }else if("login_signin".equals(method)){
//            dataResponse = doSignIn(request);
//        } else {
//            dataResponse = DataResponse.METHOD_NOT_FOUND;
//        }
        
        if("beacon_put".equals(method)) {
            dataResponse = doPutBeacon(request);
        } else if("beacon_getpartners".equals(method)) {
            dataResponse = doGetPartnerFromBeacon(request);
        } else if("beacon_addpartner".equals(method)) {
            dataResponse = doAddPartnerToBeacon(request);
        } else if("beacon_addpartners".equals(method)) {
            dataResponse = doAddPartnersToBeacon(request);
        } else if("beacon_removepartner".equals(method)) {
            dataResponse = doRemovePartnerFromBeacon(request);
        } else if("beacon_updatelocation".equals(method)) {
            dataResponse = doUpdateLocation(request);
        } else if("beacon_updatestate".equals(method)) {
            dataResponse = doUpdateBeaconState(request);
        } else if("beacon_getbeacon".equals(method)) {
            dataResponse = doGetBeacon(request);
        } else if("beacon_getlist".equals(method)) {
            dataResponse = doGetBeaconList(request);
        } else if("beacon_remove".equals(method)) {
            dataResponse = doRemoveBeacon(request);
        }
        
        
        else if("beacon_app_addapp".equals(method)) {
            dataResponse = doAddApplication(request);
        } else if("beacon_app_updateapp".equals(method)) {
            dataResponse = doUpdateApplication(request);
        } else if("beacon_app_updatename".equals(method)) {
            dataResponse = doUpdateAppName(request);
        } else if("beacon_app_updateurl".equals(method)) {
            dataResponse = doUpdateAppUrl(request);
        } else if("beacon_app_updatestate".equals(method)) {
            dataResponse = doUpdateAppState(request);
        } else if("beacon_app_remove".equals(method)) {
            dataResponse = doRemoveApp(request);
        } else if("beacon_app_getapp".equals(method)) {
            dataResponse = doGetAppInfo(request);
        } else if("beacon_app_getlist".equals(method)) {
            dataResponse = doGetAppInfoList(request);
        }
        
        else if("beacon_partner_addpartner".equals(method)) {
            dataResponse = doAddPartner(request);
        } else if("beacon_partner_updatepartner".equals(method)) {
            dataResponse = doUpdatePartner(request);
        } else if("beacon_partner_getpartner".equals(method)) {
            dataResponse = doGetPartnerInfo(request);
        } else if("beacon_partner_getlist".equals(method)) {
            dataResponse = doGetPartnerInfoList(request);
        } else if("beacon_partner_getbeacons".equals(method)) {
            dataResponse = doGetBeaconFromPartner(request);
        } else if("beacon_partner_remove".equals(method)) {
            dataResponse = doRemovePartner(request);
        } else if("beacon_partner_addbeacons".equals(method)) {
            dataResponse = doAddBeaconsToPartner(request);
        }
        
        else if("beacon_advertise_addads".equals(method)) {
            dataResponse = doAddAdvertisement(request);
        } else if("beacon_advertise_updateads".equals(method)) {
            dataResponse = doUpdateAdvertisement(request);
        } else if("beacon_advertise_getads".equals(method)) {
            dataResponse = doGetAdvertisement(request);
        } else if("beacon_advertise_remove".equals(method)) {
            dataResponse = doRemoveAdvertisement(request);
        } else if("beacon_advertise_addadstobeacon".equals(method)) {
            dataResponse = doAddAdsToBeacon(request);
        } else if("beacon_advertise_removeadsfrombeacon".equals(method)) {
            dataResponse = doRemoveAdsFromBeacon(request);
        } else if("beacon_advertise_getlist".equals(method)) {
            dataResponse = doGetAdsInfoList(request);
        } else if("beacon_advertise_getlistfrombeacon".equals(method)) {
            dataResponse = doGetAdsFromBeacon(request);
        } 
        
        if(dataResponse != null){
            dataResponse.setEscape(true);
            out(dataResponse.toString(), resp);
        }
    }
    
    private DataResponse doPutBeacon(HttpServletRequest request) {
        System.out.println("doPutBeacon");
        if (checkValidParam(request, new String[]{"uuid", "major", "minor", "lat", "lng", "enable", "name", "description"})) {
            String uuid = ServletUtil.getStringParameter(request, "uuid");
            int bMajor = ServletUtil.getIntParameter(request, "major");
            int bMinor = ServletUtil.getIntParameter(request, "minor");
            String latStr = ServletUtil.getStringParameter(request, "lat");
            String lngStr = ServletUtil.getStringParameter(request, "lng");
            int enable = ServletUtil.getIntParameter(request, "enable");
            String name = ServletUtil.getStringParameter(request, "name", "");
            String description = ServletUtil.getStringParameter(request, "description", "");
            double lat = Double.parseDouble(latStr);
            double lng = Double.parseDouble(lngStr);
            return BeaconModel.putBeaconInfo(uuid, bMajor, bMinor, lat, lng, (enable != 0), name, description );

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetPartnerFromBeacon(HttpServletRequest request) {
        System.out.println("doGetPartnerFromBeacon");
        if (checkValidParam(request, new String[]{"beaconId", "start", "length"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            int start = ServletUtil.getIntParameter(request, "start");
            int length = ServletUtil.getIntParameter(request, "length");
            System.out.println("beaconId: " + beaconId + " start: " + start + " length: " + length);
            
            return BeaconPartnerModel.doGetPartnerFromBeacon(beaconId, start, length);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddPartnerToBeacon(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"beaconId", "partnerId"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            
            return BeaconModel.doAddPartnerToBeacon(beaconId, partnerId);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddPartnersToBeacon(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"beaconId", "partnerIds"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            String partnerIds = ServletUtil.getStringParameter(request, "partnerIds");
            
            return BeaconModel.doAddPartnersToBeacon(beaconId, partnerIds);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddBeaconsToPartner(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"partnerId", "beaconIds"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            String beaconIds = ServletUtil.getStringParameter(request, "beaconIds");
            
            return BeaconModel.doAddBeaconsToPartner(partnerId, beaconIds);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    
    
    private DataResponse doRemovePartnerFromBeacon(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"beaconId", "partnerId"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            
            return BeaconModel.doRemovePartnerFromBeacon(beaconId, partnerId);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetBeacon(HttpServletRequest request) {
        System.out.println("doGetBeacon");
        if (checkValidParam(request, new String[]{"beaconId"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            
            return BeaconModel.getBeaconInfo(beaconId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetBeaconList(HttpServletRequest request) {
        System.out.println("doGetBeaconList");
        if (checkValidParam(request, new String[]{"start", "length"})) {
            int start = ServletUtil.getIntParameter(request, "start");
            int length = ServletUtil.getIntParameter(request, "length");
            
            return BeaconModel.getBeaconList(start, length);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doRemoveBeacon(HttpServletRequest request) {
        System.out.println("doRemoveBeacon");
        if (checkValidParam(request, new String[]{"beaconId"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            
            return BeaconModel.doRemoveBeacon(beaconId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateLocation(HttpServletRequest request) {
        System.out.println("doUpdateLocation");
        if (checkValidParam(request, new String[]{"beaconId", "lat", "lng"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            String latStr = ServletUtil.getStringParameter(request, "lat");
            String lngStr = ServletUtil.getStringParameter(request, "lng");
            double lat = Double.parseDouble(latStr);
            double lng = Double.parseDouble(lngStr);
            
            return BeaconModel.updateLocation(beaconId, lat, lng);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateBeaconState(HttpServletRequest request) {
        System.out.println("doUpdateBeaconState");
        if (checkValidParam(request, new String[]{"beaconId", "enable"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            
            int enable = ServletUtil.getIntParameter(request, "enable");
            return BeaconModel.updateState(beaconId, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddApplication(HttpServletRequest request) {
        System.out.println("doAddApplication");
        if (checkValidParam(request, new String[]{"name", "url"})) {
            String appName = ServletUtil.getStringParameter(request, "name");
            String url = ServletUtil.getStringParameter(request, "url");
            int enable = ServletUtil.getIntParameter(request, "enable", 1);
            return BeaconAppModel.addNewApp(appName, url, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateApplication(HttpServletRequest request) {
        System.out.println("doUpdateApplication");
        if (checkValidParam(request, new String[]{"appId", "name", "url", "enable"})) {
            long appId = ServletUtil.getLongParameter(request, "appId");
            String appName = ServletUtil.getStringParameter(request, "name");
            String url = ServletUtil.getStringParameter(request, "url");
            int enable = ServletUtil.getIntParameter(request, "enable", 1);
            return BeaconAppModel.updateApp(appId, appName, url, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doRemoveApp(HttpServletRequest request) {
        System.out.println("doRemoveApp");
        if (checkValidParam(request, new String[]{"appId"})) {
            long appId = ServletUtil.getLongParameter(request, "appId");
            
            return BeaconAppModel.doRemoveApp(appId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetAppInfo(HttpServletRequest request) {
        System.out.println("doGetAppInfo");
        if (checkValidParam(request, new String[]{"appId"})) {
            long appId = ServletUtil.getLongParameter(request, "appId");
            System.out.println("appId: " + appId);
            return BeaconAppModel.getAppInfo(appId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetAppInfoList(HttpServletRequest request) {
        System.out.println("doGetAppInfoList");
        if (checkValidParam(request, new String[]{"start", "length"})) {
            int start = ServletUtil.getIntParameter(request, "start");
            int length = ServletUtil.getIntParameter(request, "length");
            
            return BeaconAppModel.getAppInfoList(start, length);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateAppState(HttpServletRequest request) {
        System.out.println("doUpdateAppState");
        if (checkValidParam(request, new String[]{"appId", "enable"})) {
            long appId = ServletUtil.getLongParameter(request, "appId");
            
            int enable = ServletUtil.getIntParameter(request, "enable");
            return BeaconAppModel.updateState(appId, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateAppName(HttpServletRequest request) {
        System.out.println("doUpdateAppName");
        if (checkValidParam(request, new String[]{"appId", "name"})) {
            long appId = ServletUtil.getLongParameter(request, "appId");
            String name = ServletUtil.getStringParameter(request, "name");
            
            return BeaconAppModel.updateName(appId, name);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateAppUrl(HttpServletRequest request) {
        System.out.println("doUpdateAppUrl");
        if (checkValidParam(request, new String[]{"appId", "url"})) {
            long appId = ServletUtil.getLongParameter(request, "appId");
            String url = ServletUtil.getStringParameter(request, "url");
            
            return BeaconAppModel.updateUrl(appId, url);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddPartner(HttpServletRequest request) {
        System.out.println("doAddPartner");
        if (checkValidParam(request, new String[]{"name", "address", "username", "password"})) {
            String name = ServletUtil.getStringParameter(request, "name");
            String address = ServletUtil.getStringParameter(request, "address");
            String username = ServletUtil.getStringParameter(request, "username");
            String password = ServletUtil.getStringParameter(request, "password");
            int enable = ServletUtil.getIntParameter(request, "enable", 1);
            return BeaconPartnerModel.addNewPartner(name, address, (enable != 0), username, password);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdatePartner(HttpServletRequest request) {
        System.out.println("doUpdatePartner");
        if (checkValidParam(request, new String[]{"partnerId", "name", "address", "enable"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            String name = ServletUtil.getStringParameter(request, "name");
            String address = ServletUtil.getStringParameter(request, "address");
            int enable = ServletUtil.getIntParameter(request, "enable", 1);
            return BeaconPartnerModel.updatePartner(partnerId, name, address, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doRemovePartner(HttpServletRequest request) {
        System.out.println("doRemovePartner");
        if (checkValidParam(request, new String[]{"partnerId"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            
            return BeaconPartnerModel.doRemovePartner(partnerId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetPartnerInfo(HttpServletRequest request) {
        System.out.println("doGetPartnerInfo");
        if (checkValidParam(request, new String[]{"partnerId"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            
            return BeaconPartnerModel.getPartnerInfo(partnerId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetPartnerInfoList(HttpServletRequest request) {
        System.out.println("doGetAppInfoList");
        if (checkValidParam(request, new String[]{"start", "length"})) {
            int start = ServletUtil.getIntParameter(request, "start");
            int length = ServletUtil.getIntParameter(request, "length");
            
            return BeaconPartnerModel.getPartnerInfoList(start, length);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetBeaconFromPartner(HttpServletRequest request) {
        System.out.println("doGetBeaconFromPartner");
        if (checkValidParam(request, new String[]{"partnerId", "start", "length"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            int start = ServletUtil.getIntParameter(request, "start");
            int length = ServletUtil.getIntParameter(request, "length");
            
            return BeaconModel.getBeaconListFromPartner(partnerId, start, length);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdatePartnerState(HttpServletRequest request) {
        System.out.println("doUpdateAppState");
        if (checkValidParam(request, new String[]{"partnerId", "enable"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            
            int enable = ServletUtil.getIntParameter(request, "enable");
            return BeaconPartnerModel.updateState(partnerId, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdatePartnerName(HttpServletRequest request) {
        System.out.println("doUpdatePartnerName");
        if (checkValidParam(request, new String[]{"partnerId", "name"})) {
            long partnerId = ServletUtil.getLongParameter(request, "partnerId");
            String name = ServletUtil.getStringParameter(request, "name");
            
            return BeaconPartnerModel.updateName(partnerId, name);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddAdvertisement(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"title", "content"})) {
            String title = ServletUtil.getStringParameter(request, "title");
            String content = ServletUtil.getStringParameter(request, "content");
            long timeDefault = 0;
            

            
            long startTime = 0;
            long endTime = 0;
            int enable = ServletUtil.getIntParameter(request, "enable", 1);
            try {
                String startTimeString = ServletUtil.getStringParameter(request,"startTime");
                String endTimeString = ServletUtil.getStringParameter(request,"endTime");
                
                Date date;
                date = new SimpleDateFormat("dd/MM/yyyy").parse(startTimeString);
                startTime = date.getTime();
                
                date = new SimpleDateFormat("dd/MM/yyyy").parse(endTimeString);
                endTime = date.getTime();

            } catch (ParseException ex) {
            }
                                
            return AdvertisementModel.addAdvertisement(title, content, startTime, endTime, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doUpdateAdvertisement(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"adId", "title", "content"})) {
            long adId = ServletUtil.getLongParameter(request, "adId");
            String title = ServletUtil.getStringParameter(request, "title");
            String content = ServletUtil.getStringParameter(request, "content");
            long timeDefault = 0;
            long startTime = 0;
            long endTime = 0;
            int enable = ServletUtil.getIntParameter(request, "enable", 1);
             try {
                String startTimeString = ServletUtil.getStringParameter(request,"startTime");
                String endTimeString = ServletUtil.getStringParameter(request,"endTime");
                
                Date date;
                date = new SimpleDateFormat("dd/MM/yyyy").parse(startTimeString);
                startTime = date.getTime();
                
                date = new SimpleDateFormat("dd/MM/yyyy").parse(endTimeString);
                endTime = date.getTime();

            } catch (ParseException ex) {
            }
              
            return AdvertisementModel.updateAdvertisement(adId, content, content, startTime, endTime, (enable != 0));

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetAdvertisement(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"adId"})) {
            long adId = ServletUtil.getLongParameter(request, "adId");
            
            return AdvertisementModel.getAdvertisement(adId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }

    private DataResponse doRemoveAdvertisement(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"adId"})) {
            long adId = ServletUtil.getLongParameter(request, "adId");
            
            return AdvertisementModel.removeAdvertisement(adId);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doAddAdsToBeacon(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"beaconId", "adIds"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            String adIds = ServletUtil.getStringParameter(request, "adIds");
            return AdvertisementModel.addAdsToBeacon(beaconId, adIds);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doRemoveAdsFromBeacon(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"beaconId", "adIds"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            String adIds = ServletUtil.getStringParameter(request, "adIds");
            return AdvertisementModel.removeAdsFromBeacon(beaconId, adIds);
        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetAdsInfoList(HttpServletRequest request) {
        System.out.println("doGetAdsInfoList");
        if (checkValidParam(request, new String[]{"start", "length"})) {
            int start = ServletUtil.getIntParameter(request, "start");
            int length = ServletUtil.getIntParameter(request, "length");
            
            return AdvertisementModel.getAdvertisementInfoList(start, length);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    private DataResponse doGetAdsFromBeacon(HttpServletRequest request) {
        if (checkValidParam(request, new String[]{"beaconId"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            
            return AdvertisementModel.getAdvertisementsFromBeacon(beaconId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
    
}
