/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservlet;

import configuration.AppConfig;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.models.OpenApiModel;
import service.response.DataResponse;
import service.utils.ServletUtil;

/**
 *
 * @author bonpv
 */
public class UpOpenAdsController extends BaseServer{
     @Override
    protected void doProcess(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        //LogData logEntry = new LogData(request, LogConstants.CATEGORY_MAPI_API_LOGIN);
        DataResponse dataResponse = null;
        String uri = request.getRequestURI();
        String prefix = AppConfig.OPEN_ADS_PATH.replace("*", "");
        String beaconIdString = "-1";
        String deviceIdString = "-1";
        String dataStr = uri.replace(prefix, "");
        String[] dataArr = dataStr.split("/");
        if(dataArr != null) {
            if(dataArr.length >0)
                beaconIdString = dataArr[0];
            if(dataArr.length >1)
                deviceIdString = dataArr[1];
        }
        
        System.out.println("uri "+uri);
        System.out.println("beaconId "+beaconIdString);
        long beaconId = -1;
        long deviceId = -1;
         try {
             beaconId = Long.parseLong(beaconIdString);
             deviceId = Long.parseLong(deviceIdString);
         } catch (Exception e) {
             
         }
         
         if (beaconId > 0 ) {
            dataResponse =  OpenApiModel.getListAdOfBeacon(beaconId, deviceId);
         }
       
        if(dataResponse != null){
            dataResponse.setEscape(true);
            out(dataResponse.toString(), resp);
        }
    }

}
