/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.models.OpenApiModel;
import service.response.DataResponse;
import service.utils.ServletUtil;

/**
 *
 * @author prophet
 */
public class UpOpenBeaconController extends BaseServer{
    private static final Logger log = Logger.getLogger(UpOpenBeaconController.class);
    
    @Override
    protected void doProcess(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        //LogData logEntry = new LogData(request, LogConstants.CATEGORY_MAPI_API_LOGIN);
        DataResponse dataResponse = null;
        String method = ServletUtil.getStringParameter(request, "method");

        if (method != null) {
            method = method.replace(".", "_").toLowerCase();
            if ("beacon_nearbeacon".equals(method)) {
                dataResponse = doProcessNearBeacon(request);
            } else if ("beacon_listbeacon".equals(method)) {
                dataResponse = getListBeaconByLocation(request);
            } 
        }
        
        if(dataResponse != null){
            dataResponse.setEscape(true);
            out(dataResponse.toString(), resp);
        }
    }
   
    private DataResponse doProcessNearBeacon(HttpServletRequest request) {
        
        if (!checkValidParam(request, new String[]{"framework_private_deviceId", "beaconId", "appId", "deviceIdentify"})) {
            return DataResponse.MISSING_PARAM;
        }
        String private_deviceId = ServletUtil.getStringParameter(request, "framework_private_deviceId");
        long beaconId = ServletUtil.getLongParameter(request, "beaconId");
        long appId = ServletUtil.getLongParameter(request, "appId");
        String deviceIdentify = ServletUtil.getStringParameter(request, "deviceIdentify");
        return OpenApiModel.deviceNearBeacon(private_deviceId, beaconId, appId, deviceIdentify);
    }

    private DataResponse getListBeaconByLocation(HttpServletRequest request) {
        
        if (!checkValidParam(request, new String[]{"framework_private_deviceId", "longitude", "latitude", "appId"})) {
            return DataResponse.MISSING_PARAM;
        }
        String private_deviceId = ServletUtil.getStringParameter(request, "framework_private_deviceId");
        double longtitude = ServletUtil.getDoubleParameter(request, "longitude", Double.MIN_VALUE);
        double latitude = ServletUtil.getDoubleParameter(request, "latitude", Double.MIN_VALUE);
        System.out.println("deviceId: " + private_deviceId + " longtitude: " + longtitude + " latitude: " + latitude);
        long appId = ServletUtil.getLongParameter(request, "appId");
        if (longtitude == Float.MIN_VALUE || latitude == Float.MIN_VALUE) {
            log.info("invalid longitude and latitude params");
            return DataResponse.PARAM_ERROR;
        }
        return OpenApiModel.listBeaconAtLocation(private_deviceId, longtitude, latitude, appId);
    }
}
